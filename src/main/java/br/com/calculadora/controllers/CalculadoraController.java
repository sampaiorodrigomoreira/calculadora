package br.com.calculadora.controllers;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.DTOs.RespostaDTODivisao;
import br.com.calculadora.DTOs.RespostaDTOMultiplicacao;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired //instancia a classe automaticamente
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){

        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario dois numeros para serem somados");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){

        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario dois numeros para serem subtraidos");
        }else if(calculadora.getNumeros().size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario apenas dois numeros a serem subtraidos");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTOMultiplicacao multiplicar(@RequestBody Calculadora calculadora){

        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario dois numeros para serem multiplicados");
        }else if(calculadora.getNumeros().size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario apenas dois numeros a serem multiplicados");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/divisao")
    public RespostaDTODivisao divisao(@RequestBody Calculadora calculadora){

        if(calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario dois numeros para a divisao");
        }else if(calculadora.getNumeros().size() > 2){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"É necessario apenas dois numeros para a divisao");
        }
        return calculadoraService.divisao(calculadora);
    }

}
