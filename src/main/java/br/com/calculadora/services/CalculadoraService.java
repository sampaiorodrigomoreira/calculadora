package br.com.calculadora.services;

import br.com.calculadora.DTOs.RespostaDTO;
import br.com.calculadora.DTOs.RespostaDTODivisao;
import br.com.calculadora.DTOs.RespostaDTOMultiplicacao;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora){

        int resultado = 0;

        for(Integer numero: calculadora.getNumeros()){
            resultado = resultado + numero;

        }

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;

    }

    public RespostaDTO subtrair(Calculadora calculadora){

        int resultado = 0;
        int numero1 = 0;
        int numero2 = 0;
        boolean valida = false;

        for(Integer numero: calculadora.getNumeros()){

            if(valida==false){
                numero1 = numero;
                valida = true;
            }else{
                numero2 = numero;
            }
        }

        resultado = numero1 - numero2;

        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;

    }


    public RespostaDTOMultiplicacao multiplicar(Calculadora calculadora){

        double resultado = 0;
        int numero1 = 0;
        int numero2 = 0;
        boolean valida = false;

        for(Integer numero: calculadora.getNumeros()){

            if(valida==false){
                numero1 = numero;
                valida = true;
            }else{
                numero2 = numero;
                valida = false;
            }
        }

        resultado = (numero1 * numero2);

        RespostaDTOMultiplicacao respostaDTOMultiplicacao = new RespostaDTOMultiplicacao(resultado);
        return respostaDTOMultiplicacao;

    }

    public RespostaDTODivisao divisao(Calculadora calculadora){

        double resultado = 0;
        int numero1 = 0;
        int numero2 = 0;
        boolean valida = false;

        for(Integer numero: calculadora.getNumeros()){

            if(valida==false){
                numero1 = numero;
                valida = true;
            }else{
                numero2 = numero;
                valida = false;
            }
        }

        resultado = (numero1 / numero2);

        RespostaDTODivisao respostaDTODivisao = new RespostaDTODivisao(resultado);
        return respostaDTODivisao;

    }


}
