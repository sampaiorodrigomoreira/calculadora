package br.com.calculadora.DTOs;

public class RespostaDTOMultiplicacao {

    private double resultado;

    public RespostaDTOMultiplicacao() {

    }

    public RespostaDTOMultiplicacao(double resultado) {
        this.resultado = resultado;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
}
