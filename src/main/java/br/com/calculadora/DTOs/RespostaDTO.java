package br.com.calculadora.DTOs;

public class RespostaDTO {


    private int resultado;

    public RespostaDTO() {
    }

    public RespostaDTO(int resultado) {
        this.resultado = resultado;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

}
