package br.com.calculadora.DTOs;

public class RespostaDTODivisao {


    private double resultado;

    public RespostaDTODivisao() {
    }

    public RespostaDTODivisao(double resultado) {
        this.resultado = resultado;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
}
